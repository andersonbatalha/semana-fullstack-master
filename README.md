# PalpiteBox

## Descrição

- Projeto desenvolvido durante a Semana FullStack Master, durante o período de 29/06 a 03/07

## Pré-requisitos:
Você precisa do Git, NodeJS e do NPM instalado em sua máquina.

### Clonando o projeto

- Após acessar este repositório, copie o [link](https://gitlab.com/andersonbatalha/semana-fullstack-master) e
- Abra um terminal e digite `git clone https://gitlab.com/andersonbatalha/semana-fullstack-master`
- Entre na pasta do projeto

    ```
        $ cd semana-fullstack-master/
    ```

### Rodando o projeto

- Execute os comandos:

    ```
        $ npm install
        $ npm run dev
    ```

- No navegador, acesse http://localhost:3000 

## Construído com:
- [NextJS -The React Framework](https://nextjs.org/)
- [TailwindCSS - A utility-first CSS framework for rapidly building custom designs](https://tailwindcss.com/)
- [Figma - Online prototyping tool](https://www.figma.com/)
- [PurgeCSS - Remove unused CSS](https://purgecss.com/)
- [NPM - Google Spreadsheets](https://www.npmjs.com/package/google-spreadsheet)

## Licença

Este projeto é licenciado sobre a licença GNU GPLv3 - veja o arquivo LICENSE para mais informações.

## Agradecimentos

Este projeto foi construído durante as aulas da Semana Fullstack Master do DevPleno.
