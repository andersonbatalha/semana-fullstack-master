import { GoogleSpreadsheet } from 'google-spreadsheet';
import { fromBase64 } from '../../utils/base64';

// criar uma nova instância passando o id da planilha como parâmetro
const document = new GoogleSpreadsheet(process.env.SHEET_DOC_ID);

export default async (request, response) => {
    try {
        await document.useServiceAccountAuth({
            client_email: process.env.SHEET_CLIENT_EMAIL,
            private_key: fromBase64(process.env.SHEET_PRIVATE_KEY)
        }); // realiza a autenticação passando as credenciais de usuário
        await document.loadInfo(); // carrega os dados
    
        const sheet = document.sheetsByIndex[2];
    
        await sheet.loadCells('A4:B10'); // 1 - carregar os dados
        
        // retorna o valor de uma célula pelo seu endereço
        const descPromocao = sheet.getCellByA1("B4"); 
        const ativarPromocao = sheet.getCellByA1("A4");
    
        return response.end(JSON.stringify({
            "message": descPromocao.value,
            "showCoupon": ativarPromocao.value === "VERDADEIRO",
        }));
    
    } catch (error) {
        return response.end(JSON.stringify({
            "message": error,
            "showCoupon": false,
        }));
    }

};