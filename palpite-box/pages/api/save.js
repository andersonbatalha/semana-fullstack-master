import { GoogleSpreadsheet } from 'google-spreadsheet';
import { fromBase64 } from '../../utils/base64';

import moment from "moment";

// criar uma nova instância passando o id da planilha como parâmetro
const document = new GoogleSpreadsheet(process.env.SHEET_DOC_ID);

const genCupom = () => {
    const code = parseInt(moment().format('YYMMDDHHMMssSSS')).toString(16).toUpperCase();
    return `${code.substr(0,4)}-${code.substr(4,4)}-${code.substr(8,4)}`;
}

export default async (request, response) => {
    try {
        await document.useServiceAccountAuth({
            client_email: process.env.SHEET_CLIENT_EMAIL,
            private_key: fromBase64(process.env.SHEET_PRIVATE_KEY)
        }); // realiza a autenticação passando as credenciais de usuário
        await document.loadInfo(); // carrega os dados
    
        const sheet = document.sheetsByIndex[1];
        const sheetConfig = document.sheetsByIndex[2];
    
        await sheetConfig.loadCells('A4:B10'); // 1 - carregar os dados
        
        // retorna o valor de uma célula pelo seu endereço
        const descPromocao = sheetConfig.getCellByA1("B4"); 
        const ativarPromocao = sheetConfig.getCellByA1("A4");

        const data = JSON.parse(request.body);
           
        let Cupom = ''; let Promocao = '';

        if (ativarPromocao.value === 'VERDADEIRO') {
            Cupom = genCupom();
            Promocao = descPromocao.value;
        }

        const obj = {
            "Nome": data['nome'],
            "E-mail": data['email'],
            "Whatsapp": data['whatsapp'],
            "Seu comentário": data['mensagem'],
            "Nota do estabelecimento": parseInt(data['nota']),
            "Indicaria para um amigo?": data['opcao'],
            "Cupom": Cupom,
            "Promoção": Promocao,  
            "Data Preenchimento": moment().format('DD/MM/YYYY HH:mm:ss')
        };

        const row = await sheet.addRow(obj);
        response.end(JSON.stringify(obj));
    } catch (error) {   
        response.end(error.toString());
        console.log(error);
    }

};


