import React from 'react';
import Link from "next/link";
import useSWR from 'swr';
import PageTitle from '../components/pageTitle';

const fetcher = (...args) => {
    return fetch(...args).then(res => res.json())
};

const Index = () => {
    const { data, error } = useSWR('/api/get-promo', fetcher);

    return (
        <React.Fragment>
            <PageTitle title="Seja bem vindo!" />

            <div className="text-center">
                <p className="mt-16 text-xl">
                    O restaurante X sempre buscar atender melhor seus clientes.
                </p>
                <p className="text-xl">
                    Por isso, estamos abertos a ouvir suas sugestões e críticas.
                </p>

                <Link href='/pesquisa'>
                    <a>
                        <button className="text-white bg-blue-500 font-bold text-lg py-6 px-8 mt-16 hover:bg-blue-700 rounded-full shadow-xl">
                            Dar opinião ou sugestão
                        </button>
                    </a>
                </Link>

                { (!data) ? <p className="my-24 text-xl">Carregando...</p> : null }

                { (data && !error && data.showCoupon) ? 
                    <p className="my-24 text-xl">
                        { data.message }
                    </p> : <p className="my-24 text-xl"></p>
                }
            </div>

        </React.Fragment>

    );
};

export default Index;
