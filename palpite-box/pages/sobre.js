import React from 'react';
import Link from 'next/link';
import PageTitle from '../components/pageTitle';

const Sobre = () => {
    return (
        <div>
            <PageTitle title="Sobre" />
            <h1 className="text-2xl my-4">Sobre nós</h1>
            <p className="text-lg my-4">
                O (insira o nome do estabelecimento) busca satisfazer seus clientes oferecendo o melhor atendimento.
            </p>
            <Link href="/">
                <a className="mt-6">Voltar</a>
            </Link>
        </div>
    );
};

export default Sobre;