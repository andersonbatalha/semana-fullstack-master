import React, { useState } from 'react';
import Link from 'next/link';
import TextField from '../components/Form/textField';
import RadioField from '../components/Form/radioField';
import PageTitle from '../components/pageTitle';

const Pesquisa = () => {

    const [ form, setForm ] = useState({
        'nome': '',
        'email': '',
        'whatsapp': '',
        'mensagem': '',
        'nota': '',
        'opcao': ''
    });

    const [ success, setSuccess ] = useState(false);
    const [ retorno, setRetorno ] = useState({});

    const onChange = (event) => {
        console.log(event.target.name, event.target.value);

        const key = event.target.name;
        const value = event.target.value;
        setForm(old => ({
                ...old, 
                [key]: value
            })
        );
    };

    const save = async () => {
        try {
            const response = await fetch('/api/save', {
                method: 'POST',
                body: JSON.stringify(form)
            });
    
            const data = await response.json();
            setSuccess(true);
            setRetorno(data);
            
        } catch (error) {
            console.error(error);            
        };

    };

    return (
        <React.Fragment>
            <PageTitle title="Pesquisa" />
            <div className="text-center mt-6">
                <h1 className="text-4xl font-bold">Críticas e sugestões</h1>
                <p className="mt-16 text-xl">
                    O restaurante X sempre buscar atender melhor seus clientes.
                </p>
                <p className="text-xl">
                    Por isso, estamos abertos a ouvir suas sugestões e críticas.
                </p>

            </div>

            { !success ? 
                <div className="mx-64 py-8">
                    <form method="POST" className="inline">
                        <TextField 
                            label="Nome" 
                            fieldname="nome" 
                            value={ form['nome'] }
                            onChangeFunction={ onChange } />
                        <TextField 
                            label="E-mail" 
                            fieldname="email" 
                            value={ form['email'] }
                            onChangeFunction={ onChange } />
                        <TextField 
                            label="Whatsapp" 
                            fieldname="whatsapp" 
                            value={ form['whatsapp'] } 
                            onChangeFunction={ onChange } />
                        <TextField 
                            label="Sua crítica ou sugestão" 
                            fieldname="mensagem" 
                            value={ form['mensagem'] }
                            onChangeFunction={ onChange } />
                        <RadioField 
                            label="Que nota você daria para o estabelecimento?" 
                            fieldname="nota" 
                            value={ form['nota'] }
                            values={ [0,1,2,3,4,5] } 
                            onChangeFunction={ onChange } />
                        <RadioField 
                            label="Indicaria para um amigo?" 
                            fieldname="opcao" 
                            value={ form['opcao'] }
                            values={ ["Sim", "Não"] } 
                            onChangeFunction={ onChange } />
                        <div className="mb-3 mx-56">                
                            <Link href='/pesquisa'>
                                <a>
                                    <button onClick= {save} className="text-white bg-blue-500 font-bold text-lg py-6 px-8 hover:bg-blue-700 rounded-full shadow-xl">
                                        Enviar crítica ou sugestão
                                    </button>
                                </a>
                            </Link>
                        </div>
                    </form>
                </div> :

                <div className="text-center w-1/3 mx-auto my-16">
                    <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3" role="alert">
                        <p class="text-sm">Obrigado por contribuir com nossa pesquisa!</p>
                    </div>

                    <div className="text-center border-solid border-black p-6 m-3">
                        <p>Seu cupom de desconto</p>
                        <span className="text-3xl font-bold">
                            { retorno["Cupom"] }
                        </span>
                    </div>

                    <div>
                        <p className="italic font-2xl p-4 mb-5">
                            { retorno["Promoção"] }
                        </p>
                        <p className="p-4 mb-5">
                            Tire um print ou foto da tela e apresente ao garçom
                        </p>
                    </div>
                </div>
            }
        </React.Fragment>
    );
};

export default Pesquisa;