import React from 'react';
import Link from 'next/link';

import style from "./style.module.css";

const Header = () => {
    return (
        <React.Fragment>
            <header className={style.header}>
                <Link href="/">
                    <a>
                        <img src="/logo_palpitebox.png" alt="palpite-box-logo" className={ style.imgLogo } />
                    </a>
                </Link>
            </header>

            <div className={ style.links }>
                <div className={ style.link }>
                    <Link href='/sobre'>
                        <a>Sobre nós</a>
                    </Link>
                </div>
                <div className={ style.link }>
                    <Link href='/pesquisa'>
                        <a>Pesquisa</a>
                    </Link>
                </div>
                <div className={ style.link }>
                    <Link href='/contato'>
                        <a>Contato</a>
                    </Link>
                </div>
            </div>

        </React.Fragment>
    );
};

export default Header;