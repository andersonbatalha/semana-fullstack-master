import React from 'react';
import style from './style.module.css';

const RadioField = (props) => {

    const label = props.label;
    const fieldname = props.fieldname;
    const values = props.values;
    const onChangeFunction = props.onChangeFunction;
    
    const listValues = values.map(
        (value) =>  <div key={ value.toString() } className="inline-block mr-24">
                        <input type="radio" className="mr-2" name={ fieldname } value={ value } onChange={ onChangeFunction } />
                        <span className="text-sm">{ value }</span>
                    </div>
    );

    return (
        <div className="my-6" >
            <label className={ style.fieldLabel } htmlFor={ fieldname }>
                {label}
            </label>
            { listValues }
        </div>

    );

};

export default RadioField;

/**
    <div className="inline-block mr-12">
        <input type="radio" className="mr-2" name={ option } value={ option } />
        <span className="text-sm">{ option }</span>
    </div>

 */