import React from "react";
import style from './style.module.css';

const TextField = ( { label, fieldname, value, onChangeFunction } ) => {
    return (
        <div className="my-6">
            <label className={ style.fieldLabel } htmlFor={ fieldname }>{ label }</label>
            <input 
                className={ style.inputTextField + " focus:outline-none focus:shadow-outline" } 
                id={ fieldname } 
                placeholder={ label }
                type="text" 
                name={ fieldname } 
                value={ value }
                onChange={ onChangeFunction } />
        </div>
    );
    
};

export default TextField;