import React from 'react';
import style from './style.module.css';

const Footer = () => {
    return (
        <footer className={ style.footer }>
            <p className="text-white text-center text-bold">
                Projeto desenvolvido por Anderson Pontes / {' '}
                <a href="https://www.linkedin.com/in/anderson-pontes-batalha/" target="_blank">Linkedin</a> / {' '}
                <a href="https://github.com/AndersonBatalha" target="_blank">GitHub</a>
            </p>
            <div className={ style.footerImages }>
                <div>
                    <img className={ style.imgLogo } src="logo_devpleno.png" alt="Logo DevPleno" />
                </div>
                <div>
                    <img className={ style.imgLogo } src="logo_semana_fsm.png" alt="Logo Semana FullStack Master" />
                </div>
            </div>
        </footer>
    );
};

export default Footer;