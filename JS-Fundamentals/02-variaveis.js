// Atribuição de variáveis

let a = 10;
console.log(a);

console.log(nome); // undefined
var nome = "Anderson"; 

// hoisting
var nome;
console.log(nome); // undefined
nome = "Anderson"; 

// var, let e const

var n1 = 10;
let n2 = 20;
// variáveis criadas com const não podem ter seu valor modificado, exceto variáveis do tipo array e object, que podem ter seus elementos modificados
const firstName = "Anderson";
// firstName = "Marcos"; // erro
