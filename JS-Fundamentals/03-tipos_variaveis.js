// Inteiros
const idade = 25;
console.log(idade);

// Floats
const temperatura = 39.4;
console.log(temperatura);

// Strings
const firstName = 'Anderson';
const lastName = "Pontes";
const fullName = `${firstName} ${lastName}`; // template string
console.log(fullName);

// Objetos
const brasil = {
    nome: "República Federativa do Brasil",
    capital: "Brasília",
};

    // acesso aos objetos

console.log(brasil['nome']);
console.log(brasil.capital);

// Arrays

const array = [1,2,3,4,5,6,7,8,9];
console.log(array);

// Functions: first-class citizens
    // Funções podem ser atribuídas a variáveis

function soma(x,y) {
    return x+y;
}

const minhaFuncao = soma;

console.log(minhaFuncao(3,4));
