// High order functions - arrays

// forEach, map, reduce, filter

const array = [1,2,3,4,5,6,7,8,9,10];

console.log("\narray forEach");

array.forEach((value, index, array) => {
    console.log(`Item na posição ${index}: ${value}`);
    return null;
});

const array_square = array.map(value => value * value);
console.log("\narray map", array_square);

const array_total = array.reduce((previous, current) => previous + current, 0);
console.log("\narray reduce", array_total);

const array_filter = array.filter( value => value >= 5 );
console.log("\narray filter", array_filter);

