// High order functions

// É possível passar como parâmetro de uma função o resultado de outra função

const printMyName = (name) => `Hi, ${name}!`;

const execute = (f) => console.log(f);

execute(printMyName("Anderson"));