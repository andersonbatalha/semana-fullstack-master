// Tipos de declaração de funções

    // Método 1

function mensagem() {
    console.log("Olá mundo!");
};

mensagem();

function mensagem1(msg) { // função com parâmetros
    console.log(msg);
};

mensagem1("Olá pessoal!");

    // Método 2 - atribuindo uma função a uma variável

const myFunction = function soma(x,y) {
    return x + y; // uso da palavra reservada return
};

console.log(myFunction(3,4));

    // Método 3 - arrow function

const soma = (x,y) => {
    return x+y;
};

const soma1 = (x,y) => x+y; // outra forma de escrever

console.log(soma(10,20));
console.log(soma1(10,20));

