const soma = (x, y) => x + y;
const sub = (x, y) => x - y;
const mult = (x, y) => x * y;
const div = (x, y) => x / y;
const pot = (x, y) => x ** y;
const sqrt = (x) => Math.sqrt(x);

module.exports = {
    soma, sub, mult, div, pot, sqrt
}