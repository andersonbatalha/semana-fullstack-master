# JavaScript

É uma linguagem de script

* Linguagens compiladas (ex: C, C++, Java...)
    
    - Código fonte - Arquivo binário - Executado
 
        - arquivo.c => arquivo.exe 

* Linguagens de script

    - Código fonte - Executado

* ECMAScript: versão do JavaScript regulamentada pela ECMA Internation (empresa responsável por padronizar as tecnologias da informação)

    * Especificações: ES6, ES7, ES8