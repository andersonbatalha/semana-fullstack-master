const fs = require('fs');

// callback - exemplo

fs.readFile('02-variaveis.js', (error, content) => {
    console.log('\nExemplo de callback');
    if (!error) {
        console.log(content.toString());        
    } else {
        console.log(error);
    };
});

// promise
const readFile = file => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (error, content) => {
            console.log('\nExemplo de promise');
            if (!error) {
                resolve(content.toString());        
            } else {
                reject(error);
            };
        });        
    });
};


readFile('04-functions.js')
    .then(content => console.log(content))
    .catch(error => console.log(error));

// async e await
const runReadFile = async () => {
    const content = await readFile('05-hof.js');
    console.log(content);
};

runReadFile();