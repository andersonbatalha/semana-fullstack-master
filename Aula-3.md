# Semana FullStack Master

## Aula 3

* Criação do projeto

    - No terminal, criar uma pasta para o projeto e rodar `npm init`
    - No arquivo `package.json`, configurar os scripts que serão utilizados

        ```
        "scripts": {
            "next": "next",
            "dev": "next start",
            "build": "next build"
        },
        ```

    - Criar o arquivo index.js dentro da pasta pages:

        ```
        import React from 'react';

        const Index = () => {
            return <h1>Olá mundo!</h1>;
        };

        export default Index;
        ```
    
    - Rodar o projeto com `npm run dev`

* Adicionando estilos CSS com o framework TailWind
    
    - Instalar os pacote tailwindcss e suas dependências

        `npm install tailwindcss postcss-import autoprefixer`

    - Criar um arquivo de estilos (`style.css`) 

        ```
        @import 'tailwind/base';
        @import 'tailwind/components';
        @import 'tailwind/utilities'; 
        ```

    - Criar arquivo de configuração do Tailwind 

        `npx tailwindcss init`

    - Criar arquivo de configuração do PostCSS (`postcss.config.js`)

        ```
        module.exports = {
            plugins: [
                require('tailwindcss'),
                require('autoprefixer'),
            ],
        }
        ```

    - Adicionar template base, que será usada por todas as páginas. No diretório `pages/`, adicionar o arquivo `_app.js`
    

    - Criar componentes: o React permite criar componentes para determinadas partes do código (cabeçalhos e rodapé, por exemplo)

    - CSS Modules: é possível aplicar estilos CSS a apenas um componente
        
        - Dentro da pasta do componente, crie um arquivo style.module.css

        - Coloque os estilos do componente neste arquivo. Exemplo:

            ```
            .colorFooter {
                @apply bg-gray-800;
            }

            .imgLogo {
                @apply w-1/4 mx-auto;
            }

            .footer {
                @apply grid grid-cols-2 py-2 mt-2;
            }
            ```
        
        - No componente, basta importar o arquivo e trocar os valores do atributos className pelas suas respectivas classes:

            ```
            import style from './style.module.css';
            
            (...)
            
            <footer className={ style.colorFooter }
            ```