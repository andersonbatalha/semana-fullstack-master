# Semana FullStack Master - DevPleno

## Aula 1 

1. Planejamento do projeto 'Palpite Box'
    - Mockups
        - Figma

2. Web basics: Conceitos fundamentais
    * Stack (pilha): conjunto de tecnologias a serem utilizadas durante o processo de desenvolvimento
    
    * Normalmente, uma stack se divide em dois grupos: backend e frontend
        * Frontend: tudo o que é executado na máquina cliente (navegador, aplicativo móvel); aquilo que o usuário pode ver
        * Backend: tudo o que é executado no servidor; aplicação de regras de negócio; gerenciamento de dados sensíveis

    * A comunicação entre backend e frontend, por padrão ocorre através do protocolo HTTP

    * No desenvolvimento Web, as tecnologias mais utilizadas em frontend são HTML, CSS e JavaScript

    * Fluxo de comunicação do protocolo HTTP
        
        1. Frontend faz requisição (request) ao backend (através do usuário utilizando o navegador)
            
            - No navegador: http://google.com.br
            - Requisição ao servidor: GET /

        2. Backend processa a requisição e retorna uma resposta (response)

            - response (HTML, XML, JSON...)

        * O frontend SEMPRE inicia a comunicação
        * A response retorna todos os arquivos necessários ao carregamento da página (imagens, css, scripts js, )
        * Principais métodos HTTP (HTTP verbs) 
            - GET
                * Realiza a solicitação de um recurso; retorna apenas dados
            - POST
                * Modifica o estado de um recurso ou do servidor
            - PUT
                * Modifica todas as representações de um recurso; se o recurso já existe, ele é modificado; se não existe, pode ser criado
            - PATCH
                * Atualiza partes de um recurso
            - DELETE
                * Remove um recurso

3. Ferramentas e preparação do ambiente de desenvolvimento 

    * Figma
    * CodeSandbox
    * Visual Studio Code
    * Dribble
    * Node.js

#### React

* Lib User Interface (UI)
* Descreve a UI
* Atualiza a UI


