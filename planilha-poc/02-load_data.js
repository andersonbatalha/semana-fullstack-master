
// importar as credenciais de acesso do arquivo JSON
const credentials = require('./credentials.json');
const { GoogleSpreadsheet } = require('google-spreadsheet');

// criar uma nova instância passando o id da planilha como parâmetro
const document = new GoogleSpreadsheet('12_NsCMM3pAnd7X5RUfLUuhEu8I7hAUZ0pm29i9HzJlI');

// Utilizar async/await para autenticar e estabelecer a conexão
const run = async () => {
    await document.useServiceAccountAuth(credentials); // realiza a autenticação passando as credenciais de usuário
    await document.loadInfo(); // carrega os dados

    const sheet = document.sheetsByIndex[2];
    console.log(sheet.title); // título da planilha
    console.log(sheet.rowCount); // número de linhas
    console.log(sheet.columnCount); // número de colunas

    await sheet.loadCells('A4:B10'); // 1 - carregar os dados
    console.log(sheet.cellStats); // informações da planilha
    
    // retorna o valor de uma célula pelo seu endereço
    const descPromocao = sheet.getCellByA1("B4"); 
    const ativarPromocao = sheet.getCellByA1("A4");

    console.info(`${descPromocao.value} = ${ativarPromocao.value}`);
};
    
run();