const { GoogleSpreadsheet } = require('google-spreadsheet');

// importar as credenciais de acesso do arquivo JSON
const credentials = require('./credentials.json');

// criar uma nova instância passando o id da planilha como parâmetro
const document = new GoogleSpreadsheet('12_NsCMM3pAnd7X5RUfLUuhEu8I7hAUZ0pm29i9HzJlI');

// Utilizar async/await para autenticar e estabelecer a conexão
const run = async () => {
    await document.useServiceAccountAuth(credentials); // realiza a autenticação passando as credenciais de usuário
    await document.loadInfo(); // carrega os dados

    const sheet = document.sheetsByIndex[1];
       
    const row = await sheet.addRow({ // addRow adiciona apenas uma linha
        "Nome": "Anderson Pontes",
        "E-mail": "andersonpbatalha@gmail.com",
        "Whatsapp": "(47) 992514997",
        "Seu comentário": "faskfkaskfaslfklaskasflkl",
        "Nota do estabelecimento": "7.5",
        "Indicaria para um amigo?": "Sim",
        "Cupom": "abc123",
        "Promoção": "desconto",  
    });

    const rows = await sheet.addRows([ // passando um array como parâmetro de addRows é possível adicionar várias linhas
        {
            "Nome": "Anderson Batalha",
            "E-mail": "andersonpbatalha2@gmail.com",
            "Whatsapp": "(47) 992514544",
            "Seu comentário": "degkdfhdfhjhdf",
            "Nota do estabelecimento": "3",
            "Indicaria para um amigo?": "Não",
            "Cupom": "abc321",
            "Promoção": "desconto2",  
        },
        {
            "Nome": "Maria",
            "E-mail": "maria@gmail.com",
            "Whatsapp": "(47) 993514497",
            "Seu comentário": "dlsalflggrgrkgds",
            "Nota do estabelecimento": "9",
            "Indicaria para um amigo?": "Sim",
            "Cupom": "abc124",
            "Promoção": "desconto3",  
        }
    ]);

};
    
run();