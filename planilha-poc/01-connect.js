const { GoogleSpreadsheet } = require('google-spreadsheet');

// importar as credenciais de acesso do arquivo JSON
const credentials = require('./credentials.json');

// criar uma nova instância passando o id da planilha como parâmetro
const document = new GoogleSpreadsheet('12_NsCMM3pAnd7X5RUfLUuhEu8I7hAUZ0pm29i9HzJlI');

// Utilizar async/await para autenticar e estabelecer a conexão
const run = async () => {
    await document.useServiceAccountAuth(credentials); // realiza a autenticação passando as credenciais de usuário
    await document.loadInfo(); // carrega os dados

    console.log(document.title); // exibe o título da planilha
    console.log(document.locale); // exibe o idioma da planilha
};
    
run();